vv-role-nrf-zephyr-dev
=========

> ** an ansible role to initialize a lxd container with environment ready to build and flash
firmware for `nrf5340pdk`

Requirements
------------

`a working lxd and lxc on debian system`
`lxd with ubuntu:18.04 image available`
`a good heart`

`Note:` 

Add SEGGER USB lxc config device, utilizing `vendor_id` and `product_id`
(!) Must remove config device before restarting container! 

> `lxc config device add {{ container_name }} SEGGER usb vendorid=1366 productid=1055`

Example:

> `$ /opt/lxd/bin/lxc config device add my_fw_container SEGGER usb vendorid=1366 productid=1055`

Removing Device Example:

> `$ /opt/lxd/bin/lxc config device remove my_fw_container SEGGER usb`


Role Variables
--------------
`must` set the variable `container_name`. this will be the name of the initialized `lxd` container

`defaults/main.yml`

Dependencies
------------

`vv-role-lxd`
 
Example Playbook
----------------
`you want to run the role locally most likely, since the board physically needs to be connected where you are`
`#iot`

    - hosts: localhost
      roles:
         - { role: vv-role-nrf-zephyr-dev, container_name: my-fw-container }

Example of post-install tools usage:
----------------
`bash-linux-command` `./vv.build.sh <local_src_path> <remote_src_path> <nrf_board_name>`


License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a
website (HTML is not allowed).

Only Linux Debian, most likely MX Linux, is supported for the localhost metal OS.

:baby_bottle:
